package com.shumov.tm.api.service;

public interface ServiceLocator {
    IProjectService getProjectService();

    ITaskService getTaskService();

    IUserService getUserService();

    IDomainService getDomainService();

    ISessionService getSessionService();
}
