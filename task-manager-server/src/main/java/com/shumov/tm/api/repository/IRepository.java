package com.shumov.tm.api.repository;

import com.shumov.tm.api.entity.Entity;
import org.jetbrains.annotations.NotNull;

import java.util.List;

public interface IRepository<T extends Entity> {

    @NotNull
    List<T> findAll() throws Exception;

    @NotNull
    T findOne(@NotNull final String entityId) throws Exception;

    void persist(@NotNull final T entity) throws Exception;

    void merge(@NotNull final String entityId, @NotNull final T entity);

    void remove(@NotNull final String entityId) throws Exception;

    void removeAll() throws Exception;
}
