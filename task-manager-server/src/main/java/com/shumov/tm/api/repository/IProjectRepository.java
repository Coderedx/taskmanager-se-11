package com.shumov.tm.api.repository;


import com.shumov.tm.entity.Project;
import org.jetbrains.annotations.NotNull;

import java.util.List;

public interface IProjectRepository extends IRepository<Project> {

    @NotNull
    public List<Project> findAll(@NotNull final String ownerId) throws Exception;

    @NotNull
    public Project findOne(
            @NotNull final String ownerId,
            @NotNull final String entityId) throws Exception;

    public void remove(@NotNull final String ownerId, @NotNull final String entityId) throws Exception;

    public void removeAll(@NotNull final String ownerId) throws Exception;

}
