package com.shumov.tm.api.service;

import com.shumov.tm.entity.Session;
import com.shumov.tm.enumerate.Role;
import com.shumov.tm.exception.service.AccessForbiddenException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface ISessionService extends Service {

    void validate(@Nullable Session session) throws AccessForbiddenException;

    void validate(@Nullable Session session, @Nullable Role role) throws Exception;

    @Nullable Session sign(@Nullable Session session);

    @NotNull List<Session> getSessionList();

    @NotNull Session openSession(@Nullable String login, @Nullable String password) throws Exception;

    void closeSession(@Nullable Session session) throws Exception;

    void setRepository(com.shumov.tm.repository.SessionRepository repository);

    void setServiceLocator(ServiceLocator serviceLocator);

    String getServiceId();
}
