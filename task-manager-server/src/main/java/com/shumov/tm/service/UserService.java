package com.shumov.tm.service;

import com.shumov.tm.api.repository.IUserRepository;
import com.shumov.tm.api.service.IUserService;
import com.shumov.tm.entity.Session;
import com.shumov.tm.entity.User;
import com.shumov.tm.entity.dto.UserDTO;
import com.shumov.tm.enumerate.Role;
import com.shumov.tm.exception.service.ServiceLocatorNotInitialized;
import com.shumov.tm.repository.UserRepository;
import com.shumov.tm.util.HashUtil;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.jws.WebService;
import java.io.IOException;
import java.util.List;

@NoArgsConstructor
@WebService
public class UserService extends AbstractService implements IUserService {

    @NotNull
    protected IUserRepository repository = new UserRepository();

    public UserService(@NotNull final IUserRepository repository) {
        this.repository = repository;
    }

    @Override
    @NotNull
    public List<User> getUserList() throws Exception {
        return repository.findAll();
    }

    @NotNull
    public final User getUser(@Nullable final String id) throws Exception{
        isCorrectInputData(id);
        return repository.findOne(id);
    }

    @Override
    public final void createNewUser(
            @Nullable final String login,
            @Nullable final String password
    ) throws Exception {
        isCorrectLogin(login);
        isCorrectPass(password);
        @NotNull final String passwordHash = HashUtil.getMd5(password);
        @NotNull final User user = new User(login, passwordHash);
        repository.persist(user);
    }

    @Override
    public final void createNewUser(
            @Nullable final String login,
            @Nullable final String password,
            @NotNull final Role userRole
    ) throws Exception {
        isCorrectLogin(login);
        isCorrectPass(password);
        @NotNull final String passwordHash = HashUtil.getMd5(password);
        @NotNull final User user = new User(login, passwordHash, userRole);
        repository.persist(user);
    }

    @Override
    @NotNull
    public final User getUserByLogin(
        @Nullable final String login,
        @Nullable final String password
    ) throws Exception {
        isCorrectLogin(login);
        isCorrectPass(password);
        @NotNull final String passwordHash = HashUtil.getMd5(password);
        @NotNull final User user = repository.findOneByLogin(login);
        @Nullable final String userPasswordHash = user.getPassword();
        if(passwordHash.equals(userPasswordHash)) return user;
        else throw new IOException("Wrong Password");
    }

    @Override
    public final void mergeUser(@Nullable final User user) throws Exception {
        isCorrectObject(user);
        @NotNull final String id = user.getId();
        repository.merge(id, user);
    }

    @NotNull
    @Override
    public UserDTO reviewUser(@NotNull final String userId) throws Exception {
        @NotNull final User user = getUser(userId);
        @NotNull final UserDTO userDTO = new UserDTO();
        userDTO.setLogin(user.getLogin());
        userDTO.setRole(user.getRole());
        userDTO.setDescription(user.getDescription());
        return userDTO;
    }

    @Override
    public final void editDescription(@NotNull final String userId,
                                      @NotNull final String description) throws Exception{
        isCorrectInputData(userId);
        isCorrectInputData(description);
        @NotNull final User user = repository.findOne(userId);
        user.setDescription(description);
        mergeUser(user);
    }

    @Override
    public final void editPassword(@NotNull final String userId, @NotNull final String password) throws Exception  {
        @NotNull final User user = getUser(userId);
        @Nullable final String passwordHash = HashUtil.getMd5(password);
        user.setPassword(HashUtil.getMd5(password));
        mergeUser(user);
    }

    @Override
    public void setUsers(@NotNull final List<User> users) throws Exception{
        repository.removeAll();
        for (@NotNull final User user : users){
            isCorrectObject(user);
            repository.persist(user);
        }
    }

    @Override
    public final void isCorrectLogin(@Nullable final String login) throws IOException {
        if (login==null || login.isEmpty()){
            throw new IOException("Incorrect login!".toUpperCase());
        }
    }

    @Override
    public final void isCorrectPass(@Nullable final String pass) throws IOException{
        if(pass==null || pass.isEmpty()){
            throw new IOException("Incorrect password!".toUpperCase());
        }
    }

    @Override
    public final void isCorrectDescription(@Nullable final String description) throws IOException {
        if(description==null || description.isEmpty()){
            throw new IOException("Incorrect description!".toUpperCase());
        }
    }
}
