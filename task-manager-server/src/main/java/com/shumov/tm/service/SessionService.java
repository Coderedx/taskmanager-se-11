package com.shumov.tm.service;


import com.shumov.tm.api.service.ISessionService;
import com.shumov.tm.api.service.IUserService;
import com.shumov.tm.api.service.ServiceLocator;
import com.shumov.tm.entity.Session;
import com.shumov.tm.entity.User;
import com.shumov.tm.enumerate.Role;
import com.shumov.tm.exception.service.AccessForbiddenException;
import com.shumov.tm.repository.SessionRepository;
import com.shumov.tm.util.SignatureUtil;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;
import java.util.UUID;

@NoArgsConstructor
@Getter
@Setter
public class SessionService extends AbstractService implements ISessionService {

    private String serviceId = UUID.randomUUID().toString();
    @NotNull
    private SessionRepository repository = new SessionRepository();//    Изменить на интерфейс
    @Nullable
    private ServiceLocator serviceLocator;

    public SessionService(
            @NotNull final ServiceLocator serviceLocator)
    {
        this.serviceLocator = serviceLocator;
    }

    @Override
    public void validate(@Nullable final Session session) throws AccessForbiddenException {
        if(session == null) throw new AccessForbiddenException();
        if(session.getSignature()==null || session.getSignature().isEmpty()) throw new AccessForbiddenException();
        if(session.getUserId()==null || session.getUserId().isEmpty()) throw new AccessForbiddenException();
        if(session.getTimestamp()==null) throw new AccessForbiddenException();
        final Session temp = session.clone();
        if(temp==null) throw new AccessForbiddenException();
        final String signatureSource = session.getSignature();
        @Nullable final String signatureTarget = sign(temp).getSignature();
        final boolean check = signatureSource.equals(signatureTarget);
        if(!check) throw new AccessForbiddenException();
        if(!repository.contains(session.getId())) throw new AccessForbiddenException();
    }

    @Override
    public void validate(@Nullable final Session session, @Nullable Role role) throws Exception {
        if(role == null) throw new AccessForbiddenException();
        validate(session);
        if(session == null) throw new AccessForbiddenException();
        @Nullable final String userId = session.getUserId();
        if(serviceLocator==null) throw new AccessForbiddenException();
        @Nullable final User user = serviceLocator.getUserService().getUser(userId);
        if(user == null) throw new AccessForbiddenException();
        if(user.getRole()==null) throw new AccessForbiddenException();
        if(!role.equals(user.getRole())) throw new AccessForbiddenException();
    }

    @Override
    @Nullable
    public Session sign(@Nullable final Session session){
        if(session == null) return null;
        session.setSignature(null);
        @Nullable final String signature = SignatureUtil.sign(session, "WakeUp,Neo", 188);
        session.setSignature(signature);
        return session;
    }

    @Override
    @NotNull
    public List<Session> getSessionList(){
        return repository.findAll();
    }

    @Override
    @NotNull
    public Session openSession(@Nullable final String login, @Nullable final String password) throws Exception{
        if(serviceLocator == null) throw new AccessForbiddenException();
        @NotNull final IUserService userService = serviceLocator.getUserService();
        @NotNull final User user = userService.getUserByLogin(login,password);
        @NotNull final Session session = new Session();
        session.setUserId(user.getId());
        session.setOwnerId(serviceId);
        session.setTimestamp(1000L); //Вынести в параметры сервера, отдельным классом констант.
        @Nullable final Session signedSession = sign(session);
        if(signedSession == null) throw new AccessForbiddenException();
        repository.persist(signedSession);
        return signedSession;
    }

    @Override
    public void closeSession(@Nullable final Session session) throws Exception {
        validate(session);
        repository.remove(session.getId());
    }




}
