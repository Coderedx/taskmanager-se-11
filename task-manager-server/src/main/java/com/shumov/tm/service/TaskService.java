package com.shumov.tm.service;

import com.shumov.tm.api.repository.ITaskRepository;
import com.shumov.tm.api.service.IProjectService;
import com.shumov.tm.api.service.ITaskService;
import com.shumov.tm.api.service.ServiceLocator;
import com.shumov.tm.comparator.entity.TaskComparator;
import com.shumov.tm.entity.Task;
import com.shumov.tm.enumerate.ExecutionStatus;
import com.shumov.tm.enumerate.SortMethod;
import com.shumov.tm.exception.service.ServiceLocatorNotInitialized;
import com.shumov.tm.repository.TaskRepository;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.jws.WebService;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@NoArgsConstructor
@WebService
public class TaskService extends AbstractService implements ITaskService {

    @Nullable
    private ServiceLocator serviceLocator;
    @NotNull
    protected ITaskRepository repository = new TaskRepository();

    public TaskService(@NotNull final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    public void createTask(
        @Nullable final String ownerId,
        @Nullable final String name,
        @Nullable final String projectId
    ) throws Exception {
        isCorrectInputData(ownerId);
        isCorrectInputData(name);
        isCorrectInputData(projectId);
        if(serviceLocator == null) throw new ServiceLocatorNotInitialized();
        @NotNull final IProjectService projectService = serviceLocator.getProjectService();
        projectService.getProject(projectId);
        @NotNull final Task task = new Task(ownerId, name, projectId);
        repository.persist(task);
    }

    @Override
    public void addTaskInProject(
            @Nullable final String taskId,
            @Nullable final Task task
    ) throws Exception {
        isCorrectInputData(taskId);
        isCorrectObject(task);
        repository.merge(taskId,task);
    }

    @Override
    public void addTaskInProject(
            @Nullable final String ownerId,
            @Nullable final String projectId,
            @Nullable final String taskId) throws Exception {
        if(serviceLocator == null) throw new ServiceLocatorNotInitialized();
        isCorrectInputData(ownerId);
        isCorrectInputData(taskId);
        isCorrectInputData(projectId);
        @NotNull final IProjectService projectService = serviceLocator.getProjectService();
        projectService.getProject(ownerId, projectId);
        @NotNull final Task task = getTask(ownerId, taskId);
        task.setIdProject(projectId);
        repository.merge(taskId, task);
    }

    @Override
    @NotNull
    public List<Task> getTaskList() throws Exception {
        return repository.findAll();
    }

    @Override
    @NotNull
    public List<Task> getTaskList(@Nullable final String ownerId) throws Exception {
        isCorrectInputData(ownerId);
        return repository.findAll(ownerId);
    }

    @Override
    public void setTasks(@NotNull final List<Task> tasks) throws Exception{
        repository.removeAll();
        for (@NotNull final Task task : tasks){
            isCorrectObject(task);
            repository.persist(task);
        }
    }

    @Override
    @NotNull
    public List<Task> getTaskSortedList(
            @Nullable final String ownerId,
            @Nullable final String method
    ) throws Exception {
        isCorrectInputData(ownerId);
        isCorrectInputData(method);
        @NotNull final List<Task> list = repository.findAll(ownerId);
        sortTasks(list,method);
        return list;
    }

    @Override
    @NotNull
    public Task getTask(@Nullable final String taskId) throws Exception {
        isCorrectInputData(taskId);
        return repository.findOne(taskId);
    }

    @Override
    @NotNull
    public Task getTask(@Nullable final String ownerId, @Nullable final String taskId) throws Exception {
        isCorrectInputData(ownerId);
        isCorrectInputData(taskId);
        return repository.findOne(ownerId, taskId);
    }

    @Override
    @NotNull
    public List<Task> getTasksOfProject(@Nullable final String projectId) throws Exception {
        isCorrectInputData(projectId);
        @NotNull final List<Task> tasks = new ArrayList<>();
        for (@NotNull final Task task : repository.findAll()) {
            @NotNull final String taskProjectId = task.getIdProject();
            if(projectId.equals(taskProjectId)){
                tasks.add(task);
            }
        }
        return tasks;
    }

    @Override
    @NotNull
    public List<Task> getTasksOfProject (
        @Nullable final String ownerId,
        @Nullable final String projectId
    ) throws Exception {
        isCorrectInputData(ownerId);
        isCorrectInputData(projectId);
        @NotNull final List<Task> tasks = new ArrayList<>();
        for (@NotNull final Task task : repository.findAll(ownerId)) {
            @NotNull final String taskProjectId = task.getIdProject();
            if(projectId.equals(taskProjectId)){
                tasks.add(task);
            }
        }
        return tasks;
    }

    @Override
    public void clearTasks() throws Exception {
        repository.removeAll();
    }

    @Override
    public void clearTasks(@Nullable final String ownerId) throws Exception{
        isCorrectInputData(ownerId);
        repository.removeAll(ownerId);
    }

    @Override
    public void editTaskNameById(
        @Nullable final String taskId,
        @Nullable final String name
    ) throws Exception {
        isCorrectInputData(taskId);
        isCorrectInputData(name);
        @NotNull final Task task = repository.findOne(taskId);
        task.setName(name);
        repository.merge(taskId, task);
    }

    @Override
    public void editTaskNameById(
        @Nullable final String ownerId,
        @Nullable final String taskId,
        @Nullable final String name
    ) throws Exception {
        isCorrectInputData(ownerId);
        isCorrectInputData(taskId);
        isCorrectInputData(name);
        @NotNull final Task task = repository.findOne(ownerId, taskId);
        task.setName(name);
        repository.merge(taskId, task);
    }

    @Override
    public void editTaskDate(
        @Nullable String ownerId,
        @Nullable String taskId,
        @Nullable String start,
        @Nullable String finish
    ) throws Exception {
        isCorrectInputData(ownerId);
        isCorrectInputData(taskId);
        @NotNull final Date dateStart = parseDate(start);
        @NotNull final Date dateFinish = parseDate(finish);
        @NotNull final Task task = repository.findOne(ownerId, taskId);
        task.setDateStart(dateStart);
        task.setDateFinish(dateFinish);
        repository.merge(taskId, task);
    }

    @Override
    public void editTaskStatus(
        @Nullable String ownerId,
        @Nullable String taskId,
        @Nullable String status
    ) throws Exception {
        isCorrectInputData(ownerId);
        isCorrectInputData(taskId);
        isCorrectInputData(status);
        @NotNull final Task task = repository.findOne(ownerId, taskId);
        task.setStatus(taskStatus(status));
        repository.merge(taskId, task);
    }

    @Override
    public void removeTaskById(@Nullable final String id) throws Exception{
        isCorrectInputData(id);
        repository.remove(id);
    }

    @Override
    public void removeTaskById(
        @Nullable final String ownerId,
        @Nullable final String taskId
    ) throws Exception {
        isCorrectInputData(ownerId);
        isCorrectInputData(taskId);
        repository.remove(ownerId, taskId);
    }

    @Override
    @NotNull
    public List<Task> searchTasks(@NotNull final String ownerId, @NotNull final String term) throws Exception {
        @NotNull final List<Task> listTasks = repository.findAll(ownerId);
        @NotNull final List<Task> listResult = new ArrayList<>();
        for (@NotNull final Task task : listTasks) {
            if(task.getName().contains(term) || task.getDescription().contains(term)){
                listResult.add(task);
            }
        }
        return listResult;
    }

    private void sortTasks(@NotNull final List<Task> list, @NotNull final String method){
        switch (method.toLowerCase()){
            case "create" :
                list.sort(new TaskComparator(SortMethod.BY_CREATE_DATE));
                break;
            case "start" :
                list.sort(new TaskComparator(SortMethod.BY_START_DATE));
                break;
            case "finish" :
                list.sort(new TaskComparator(SortMethod.BY_FINISH_DATE));
                break;
            case "status" :
                list.sort(new TaskComparator(SortMethod.BY_STATUS));
                break;
            default:
                System.out.println("[you have not selected a sorting method]".toUpperCase());
        }
    }

    @NotNull
    private ExecutionStatus taskStatus(@NotNull final String status) throws Exception  {
        switch (status.toLowerCase()){
            case "planned" : return ExecutionStatus.PLANNED;
            case "progress" : return ExecutionStatus.IN_PROGRESS;
            case "done" : return ExecutionStatus.DONE;
            default:
                throw new IOException("[you have not selected a status]".toUpperCase());
        }
    }
}
