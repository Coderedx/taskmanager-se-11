package com.shumov.tm.service;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.dataformat.xml.ser.ToXmlGenerator;
import com.shumov.tm.api.service.*;
import com.shumov.tm.entity.Project;
import com.shumov.tm.entity.Session;
import com.shumov.tm.entity.Task;
import com.shumov.tm.entity.User;
import com.shumov.tm.entity.domain.Domain;
import com.shumov.tm.entity.dto.UserDTO;
import com.shumov.tm.enumerate.Role;
import com.shumov.tm.exception.service.ServiceLocatorNotInitialized;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.eclipse.persistence.jaxb.JAXBContextFactory;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Getter
@Setter
@NoArgsConstructor
public class AdminService extends AbstractService implements IAdminService {

    @Nullable
    private ServiceLocator serviceLocator;

    public AdminService(@NotNull ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    public void projectClearDB() throws Exception {
        if(serviceLocator == null) throw new ServiceLocatorNotInitialized();
        @NotNull final IProjectService projectService = serviceLocator.getProjectService();
        projectService.clearProjects();
    }

    @Override
    public void projectEditName(@Nullable final String projectId, @Nullable final String name) throws Exception{
        if(serviceLocator == null) throw new ServiceLocatorNotInitialized();
        isCorrectInputData(projectId);
        isCorrectInputData(name);
        @NotNull final IProjectService projectService = serviceLocator.getProjectService();
        projectService.editProjectNameById(projectId, name);
    }

    @Override
    @NotNull
    public List<Project> getProjectList() throws Exception {
        if(serviceLocator == null) throw new ServiceLocatorNotInitialized();
        @NotNull final IProjectService projectService = serviceLocator.getProjectService();
        return projectService.getProjectList();
    }

    @Override
    public void removeProjectById(@Nullable final String projectId) throws Exception {
        if(serviceLocator == null) throw new ServiceLocatorNotInitialized();
        isCorrectInputData(projectId);
        @NotNull final IProjectService projectService = serviceLocator.getProjectService();
        projectService.removeProjectById(projectId);
    }

    @Override
    @NotNull
    public List<Task> getProjectTasksById(@Nullable final String projectId) throws Exception {
        if(serviceLocator == null) throw new ServiceLocatorNotInitialized();
        isCorrectInputData(projectId);
        @NotNull final IProjectService projectService = serviceLocator.getProjectService();
        projectService.getProject(projectId);
        @NotNull final ITaskService taskService = serviceLocator.getTaskService();
        return taskService.getTasksOfProject(projectId);
    }

    // Методы для Task:

    @Override
    public void addTaskInProject(@Nullable final String projectId, @Nullable final String taskId) throws Exception {
        if(serviceLocator == null) throw new ServiceLocatorNotInitialized();
        isCorrectInputData(projectId);
        isCorrectInputData(taskId);
        @NotNull final IProjectService projectService = serviceLocator.getProjectService();
        projectService.getProject(projectId);
        @NotNull final ITaskService taskService = serviceLocator.getTaskService();
        @NotNull final Task task = taskService.getTask(taskId);
        task.setIdProject(projectId);
        taskService.addTaskInProject(taskId, task);
    }

    @Override
    public void taskClearDB() throws Exception {
        if(serviceLocator == null) throw new ServiceLocatorNotInitialized();
        @NotNull final ITaskService taskService = serviceLocator.getTaskService();
        taskService.clearTasks();
    }

    @Override
    public void taskEditName(@Nullable final String taskId, @Nullable final String name) throws Exception{
        if(serviceLocator == null) throw new ServiceLocatorNotInitialized();
        isCorrectInputData(taskId);
        isCorrectInputData(name);
        @NotNull final ITaskService taskService = serviceLocator.getTaskService();
        taskService.editTaskNameById(taskId, name);
    }

    @Override
    @NotNull
    public List<Task> getTaskList() throws Exception {
        if(serviceLocator == null) throw new ServiceLocatorNotInitialized();
        @NotNull final ITaskService taskService = serviceLocator.getTaskService();
        return taskService.getTaskList();
    }

    @Override
    public void removeTaskById(@Nullable final String taskId) throws Exception {
        if(serviceLocator == null) throw new ServiceLocatorNotInitialized();
        isCorrectInputData(taskId);
        @NotNull final ITaskService taskService = serviceLocator.getTaskService();
        taskService.removeTaskById(taskId);
    }

    // Методы для User

    @Override
    public void regAdmin(@Nullable final String login, @Nullable final String pass) throws Exception {
        if(serviceLocator == null) throw new ServiceLocatorNotInitialized();
        isCorrectInputData(login);
        isCorrectInputData(pass);
        @NotNull final IUserService userService = serviceLocator.getUserService();
        userService.createNewUser(login, pass, Role.ADMIN);
    }

    @Override
    @NotNull
    public UserDTO reviewUser(@NotNull final Session session) throws Exception {
        if(serviceLocator == null) throw new ServiceLocatorNotInitialized();
        @NotNull final IUserService userService = serviceLocator.getUserService();
        @NotNull final User user = userService.getUser(session.getUserId());
        @NotNull final UserDTO userDTO = new UserDTO();
        userDTO.setLogin(user.getLogin());
        userDTO.setRole(user.getRole());
        userDTO.setDescription(user.getDescription());
        return userDTO;
    }

    // Методы для сохранения базы данных

    @Override
    public void dataLoadBin() throws Exception {
        if(serviceLocator == null) return;
        @NotNull final File workingFolder = new File("src"+File.separator+"resources");
        @NotNull final File domainFile = new File(workingFolder, "domain.bin");
        @NotNull final FileInputStream fileInputStream = new FileInputStream(domainFile);
        @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
        @NotNull final Domain domain = (Domain) objectInputStream.readObject();
        @NotNull final IDomainService domainService = serviceLocator.getDomainService();
        domainService.load(domain);
    }

    @Override
    public void dataSaveBin() throws Exception {
        if(serviceLocator == null) return;
        @NotNull final File workingFolder = new File("src"+File.separator+"resources");
        if (!workingFolder.exists()) workingFolder.mkdir();
        @NotNull final File domainFile = new File(workingFolder, "domain.bin");
        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(domainFile);
        @NotNull final ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
        if(serviceLocator == null) return;
        @NotNull final IDomainService domainService = serviceLocator.getDomainService();
        @NotNull final Domain domain = new Domain();
        domainService.export(domain);
        objectOutputStream.writeObject(domain);
    }

    @Override
    public void dataLoadFasterJSON() throws Exception {
        if(serviceLocator == null) return;
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final File workingFolder = new File("src"+File.separator+"resources");
        @NotNull final File domainFile = new File(workingFolder, "domain.json");
        @NotNull final Domain domain = objectMapper.readValue(domainFile,Domain.class);
        @NotNull final IDomainService domainService = serviceLocator.getDomainService();
        domainService.load(domain);
    }

    @Override
    public void dataLoadFasterXML() throws Exception {
        if(serviceLocator == null) return;
        @NotNull final XmlMapper xmlMapper = new XmlMapper();
        @NotNull final File workingFolder = new File("src"+File.separator+"resources");
        @NotNull final File domainFile = new File(workingFolder, "domain.xml");
        @NotNull final Domain domain = xmlMapper.readValue(domainFile,Domain.class);
        @NotNull final IDomainService domainService = serviceLocator.getDomainService();
        domainService.load(domain);
    }

    @Override
    public void dataSaveFasterJSON() throws Exception {
        if(serviceLocator == null) return;
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final ObjectWriter objectWriter = objectMapper.writerWithDefaultPrettyPrinter();
        @NotNull final Domain domain = new Domain();
        @NotNull final IDomainService domainService = serviceLocator.getDomainService();
        domainService.export(domain);
        @NotNull final String json = objectWriter.writeValueAsString(domain);
        @NotNull final byte[] data = json.getBytes(StandardCharsets.UTF_8);
        @NotNull final File workingFolder = new File("src"+File.separator+"resources");
        if (!workingFolder.exists()) workingFolder.mkdir();
        @NotNull final File domainFile = new File(workingFolder, "domain.json");
        Files.write(domainFile.toPath(), data);
    }

    @Override
    public void dataSaveFasterXML() throws Exception {
        if(serviceLocator == null) return;
        @NotNull final XmlMapper xmlMapper = new XmlMapper();
        xmlMapper.configure(ToXmlGenerator.Feature.WRITE_XML_1_1, true);
        @NotNull final ObjectWriter objectWriter = xmlMapper.writerWithDefaultPrettyPrinter();
        @NotNull final Domain domain = new Domain();
        @NotNull final IDomainService domainService = serviceLocator.getDomainService();
        domainService.export(domain);
        @NotNull final String xml = objectWriter.writeValueAsString(domain);
        @NotNull final byte[] data = xml.getBytes(StandardCharsets.UTF_8);
        @NotNull final File workingFolder = new File("src"+File.separator+"resources");
        if (!workingFolder.exists()) workingFolder.mkdir();
        @NotNull final File domainFile = new File(workingFolder, "domain.xml");
        Files.write(domainFile.toPath(), data);
    }

    @Override
    public void dataLoadJaxBJSON() throws Exception {
        if(serviceLocator == null) return;
        final @NotNull Map<String, Object> properties = new HashMap<>();
        properties.put("eclipselink.media-type", "application/json");
        @NotNull final JAXBContext context = JAXBContextFactory.createContext(new Class[]{Domain.class}, properties);
        @NotNull final Unmarshaller unmarshaller = context.createUnmarshaller();
        @NotNull final File workingFolder = new File("src"+File.separator+"resources");
        @NotNull final File domainFile = new File(workingFolder, "domain.json");
        @NotNull final Domain domain = (Domain) unmarshaller.unmarshal(domainFile);
        @NotNull final IDomainService domainService = serviceLocator.getDomainService();
        domainService.load(domain);
    }

    @Override
    public void dataLoadJaxBXML() throws Exception {
        if(serviceLocator == null) return;
        @NotNull final JAXBContext context = JAXBContextFactory.createContext(new Class[]{Domain.class}, new HashMap());
        @NotNull final Unmarshaller unmarshaller = context.createUnmarshaller();
        @NotNull final File workingFolder = new File("src"+File.separator+"resources");
        @NotNull final File domainFile = new File(workingFolder, "domain.xml");
        @NotNull final Domain domain = (Domain) unmarshaller.unmarshal(domainFile);
        @NotNull final IDomainService domainService = serviceLocator.getDomainService();
        domainService.load(domain);
    }

    @Override
    public void dataSaveJaxBJSON() throws Exception {
        if(serviceLocator == null) return;
        @NotNull final Map<String, Object> properties = new HashMap<>();
        properties.put("eclipselink.media-type", "application/json");
        @NotNull final JAXBContext context = JAXBContextFactory.createContext(new Class[]{Domain.class}, properties);
        @NotNull final Marshaller marshaller = context.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        @NotNull final File workingFolder = new File("src"+File.separator+"resources");
        if (!workingFolder.exists()) workingFolder.mkdir();
        @NotNull final File domainFile = new File(workingFolder, "domain.json");
        @NotNull final Domain domain = new Domain();
        @NotNull final IDomainService domainService = serviceLocator.getDomainService();
        domainService.export(domain);
        marshaller.marshal(domain, domainFile);
    }

    @Override
    public void dataSaveJaxBXML() throws Exception {
        if(serviceLocator == null) return;
        @NotNull final JAXBContext context = JAXBContextFactory.createContext(new Class[]{Domain.class}, new HashMap());
        @NotNull final Marshaller marshaller = context.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        @NotNull final File workingFolder = new File("src"+File.separator+"resources");
        if (!workingFolder.exists()) workingFolder.mkdir();
        @NotNull final File domainFile = new File(workingFolder, "domain.xml");
        @NotNull final Domain domain = new Domain();
        @NotNull final IDomainService domainService = serviceLocator.getDomainService();
        domainService.export(domain);
        marshaller.marshal(domain, domainFile);
    }


}
