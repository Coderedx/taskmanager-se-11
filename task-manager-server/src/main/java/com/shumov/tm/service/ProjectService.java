package com.shumov.tm.service;

import com.shumov.tm.api.repository.IProjectRepository;
import com.shumov.tm.api.service.IProjectService;
import com.shumov.tm.api.service.ITaskService;
import com.shumov.tm.api.service.ServiceLocator;
import com.shumov.tm.comparator.entity.ProjectComparator;
import com.shumov.tm.entity.Project;
import com.shumov.tm.entity.Task;
import com.shumov.tm.enumerate.ExecutionStatus;
import com.shumov.tm.enumerate.SortMethod;
import com.shumov.tm.exception.service.ServiceLocatorNotInitialized;
import com.shumov.tm.repository.ProjectRepository;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.jws.WebService;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@NoArgsConstructor
@WebService
public class ProjectService extends AbstractService implements IProjectService {


    @Nullable
    private ServiceLocator serviceLocator;
    @NotNull
    protected IProjectRepository repository = new ProjectRepository();

    public ProjectService(@NotNull final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    public void createProject(@Nullable final String ownerId, @Nullable final String name) throws Exception  {
        isCorrectInputData(ownerId);
        isCorrectInputData(name);
        @NotNull final Project project = new Project(ownerId, name);
        repository.persist(project);
    }

    @Override
    @NotNull
    public List<Project> getProjectList() throws Exception {
        return repository.findAll();
    }

    @Override
    @NotNull
    public List<Project> getProjectList(@Nullable final String ownerId) throws Exception {
        isCorrectInputData(ownerId);
        return repository.findAll(ownerId);
    }

    @NotNull
    @Override
    public List<Task> getProjectTasksById(@Nullable final String ownerId,
                                          @Nullable final String projectId) throws Exception {
        if(serviceLocator == null) throw new ServiceLocatorNotInitialized();
        @NotNull final IProjectService projectService = serviceLocator.getProjectService();
        projectService.getProject(projectId);
        @NotNull final ITaskService taskService = serviceLocator.getTaskService();
        return taskService.getTasksOfProject(ownerId,projectId);
    }

    @Override
    @NotNull
    public List<Project> getProjectSortedList(
            @Nullable final String ownerId,
            @Nullable final String method
    ) throws Exception {
        isCorrectInputData(ownerId);
        isCorrectInputData(method);
        @NotNull final List<Project> list = repository.findAll(ownerId);
        sortProjects(list,method);
        return list;
    }

    @Override
    @NotNull
    public Project getProject(@Nullable final String projectId) throws Exception {
        isCorrectInputData(projectId);
        return repository.findOne(projectId);
    }

    @Override
    @NotNull
    public final Project getProject(@Nullable final String ownerId,
                                    @Nullable final String projectId) throws Exception {
        isCorrectInputData(ownerId);
        isCorrectInputData(projectId);
        return repository.findOne(ownerId, projectId);
    }

    @Override
    public final void editProjectNameById(@Nullable final String projectId,
                                          @Nullable final String name) throws Exception {
        isCorrectInputData(projectId);
        isCorrectInputData(name);
        @NotNull final Project project = repository.findOne(projectId);
        project.setName(name);
        repository.merge(projectId, project);
    }

    @Override
    public final void editProjectNameById(
        @Nullable final String ownerId,
        @Nullable final String projectId,
        @Nullable final String name
    ) throws Exception {
        isCorrectInputData(ownerId);
        isCorrectInputData(projectId);
        isCorrectInputData(name);
        @NotNull final Project project = repository.findOne(ownerId, projectId);
        project.setName(name);
        repository.merge(projectId, project);
    }

    @Override
    public void editProjectDate(
        @Nullable String ownerId,
        @Nullable String projectId,
        @Nullable String start,
        @Nullable String finish
    ) throws Exception {
        isCorrectInputData(ownerId);
        isCorrectInputData(projectId);
        @NotNull final Date dateStart = parseDate(start);
        @NotNull final Date dateFinish = parseDate(finish);
        @NotNull final Project project = repository.findOne(ownerId, projectId);
        project.setDateStart(dateStart);
        project.setDateFinish(dateFinish);
        repository.merge(projectId, project);
    }

    @Override
    public void editProjectStatus(
        @Nullable String ownerId,
        @Nullable String projectId,
        @Nullable String status
    ) throws Exception {
        isCorrectInputData(ownerId);
        isCorrectInputData(projectId);
        isCorrectInputData(status);
        @NotNull final Project project = repository.findOne(ownerId, projectId);
        project.setStatus(projectStatus(status));
        repository.merge(projectId, project);
    }

    @Override
    public final void removeProjectById(@Nullable final String projectId) throws Exception {
        isCorrectInputData(projectId);
        repository.remove(projectId);
        if(serviceLocator == null) return;
        @NotNull ITaskService taskService = serviceLocator.getTaskService();
        @NotNull final List<Task> list = taskService.getTaskList();
        for (@NotNull final Task task : list) {
            @NotNull final String taskProjectId = task.getIdProject();
            if(!list.isEmpty() && projectId.equals(taskProjectId)) {
                @NotNull final String taskId = task.getId();
                taskService.removeTaskById(taskId);
            }
        }
    }

    @Override
    public final void removeProjectById (
        @Nullable final String ownerId,
        @Nullable final String projectId
    ) throws Exception {
        isCorrectInputData(ownerId);
        isCorrectInputData(projectId);
        repository.remove(ownerId, projectId);
        if(serviceLocator == null) throw new ServiceLocatorNotInitialized();
        @NotNull ITaskService taskService = serviceLocator.getTaskService();
        @NotNull final List<Task> list = taskService.getTaskList(ownerId);
        for (@NotNull final Task task : list) {
            @NotNull final String taskProjectId = task.getIdProject();
            if(projectId.equals(taskProjectId)){
                @NotNull final String taskId = task.getId();
                taskService.removeTaskById(ownerId, taskId);
            }
        }
    }

    @Override
    public final void clearProjects() throws Exception {
        repository.removeAll();
        if(serviceLocator == null) throw new ServiceLocatorNotInitialized();
        @NotNull ITaskService taskService = serviceLocator.getTaskService();
        taskService.clearTasks();
    }

    @Override
    public final void clearProjects(@Nullable final String ownerId) throws Exception {
        isCorrectInputData(ownerId);
        repository.removeAll(ownerId);
        if(serviceLocator == null) throw new ServiceLocatorNotInitialized();
        @NotNull ITaskService taskService = serviceLocator.getTaskService();
        taskService.clearTasks(ownerId);
    }

    @Override
    public void setProjects(@NotNull final List<Project> projects) throws Exception{
        repository.removeAll();
        for (@NotNull final Project project : projects){
            isCorrectObject(project);
            repository.persist(project);
        }
    }

    @Override
    @NotNull
    public List<Project> searchProjects(@Nullable final String ownerId, @Nullable final String term) throws Exception {
        isCorrectInputData(ownerId);
        isCorrectInputData(term);
        @NotNull final List<Project> listProjects = repository.findAll(ownerId);
        @NotNull final List<Project> listResult = new ArrayList<>();
        for (@NotNull final Project project : listProjects) {
            if(project.getName().contains(term) || project.getDescription().contains(term)){
                listResult.add(project);
            }
        }
        return listResult;
    }

    private void sortProjects(@NotNull final List<Project> list, @NotNull final String method) throws Exception{
        switch (method.toLowerCase()){
            case "create" :
                list.sort(new ProjectComparator(SortMethod.BY_CREATE_DATE));
                break;
            case "start" :
                list.sort(new ProjectComparator(SortMethod.BY_START_DATE));
                break;
            case "finish" :
                list.sort(new ProjectComparator(SortMethod.BY_FINISH_DATE));
                break;
            case "status" :
                list.sort(new ProjectComparator(SortMethod.BY_STATUS));
                break;
            default:
                System.out.println("[you have not selected a sorting method]".toUpperCase());
        }
    }

    @NotNull
    private ExecutionStatus projectStatus(@NotNull final String status) throws Exception  {
        switch (status.toLowerCase()){
            case "planned" : return ExecutionStatus.PLANNED;
            case "progress" : return ExecutionStatus.IN_PROGRESS;
            case "done" : return ExecutionStatus.DONE;
            default:
                throw new IOException("[you have not selected a status]".toUpperCase());
        }
    }
}
