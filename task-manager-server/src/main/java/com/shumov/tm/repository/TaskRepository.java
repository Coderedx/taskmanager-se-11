package com.shumov.tm.repository;

import com.shumov.tm.api.repository.ITaskRepository;
import com.shumov.tm.entity.Task;
import com.shumov.tm.exception.entity.EntityIsAlreadyExistException;
import com.shumov.tm.exception.entity.EntityListIsEmptyException;
import com.shumov.tm.exception.entity.EntityNotExistException;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
public class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {

    @Override
    @NotNull
    public List<Task> findAll() throws Exception {
        final boolean mapIsEmpty = entityMap.isEmpty();
        if(mapIsEmpty) return new ArrayList<>();
        else return new ArrayList<>(entityMap.values());
    }

    @Override
    @NotNull
    public List<Task> findAll(@NotNull final String ownerId) throws Exception {
        @NotNull final List<Task> list = new ArrayList<>();
        for (@NotNull final Task entity: findAll()) {
            @NotNull final String entityOwnerId = entity.getOwnerId();
            if(entityOwnerId.equals(ownerId)) list.add(entity);
        }
        final boolean listIsEmpty = list.isEmpty();
        if(listIsEmpty) return new ArrayList<>();
        else return list;
    }

    @Override
    @NotNull
    public Task findOne(@NotNull final String entityId) throws Exception {
        final boolean mapIsEmpty = entityMap.isEmpty();
        if(mapIsEmpty) throw new EntityListIsEmptyException();
        final boolean mapContainsKey = entityMap.containsKey(entityId);
        if(mapContainsKey) return entityMap.get(entityId);
        else throw new EntityNotExistException();
    }

    @Override
    @NotNull
    public Task findOne(
            @NotNull final String ownerId,
            @NotNull final String entityId
    ) throws Exception {
        @NotNull final Task entity = findOne(entityId);
        @NotNull final String entityOwnerId = entity.getOwnerId();
        final boolean equality = entityOwnerId.equals(ownerId);
        if (equality) return entity;
        else throw new EntityNotExistException();
    }

    @Override
    public void persist(@NotNull final Task entity) throws Exception {
        @NotNull final String entityId = entity.getId();
        final boolean mapContainsKey = entityMap.containsKey(entityId);
        if(mapContainsKey) throw new EntityIsAlreadyExistException();
        else entityMap.put(entity.getId(), entity);
    }

    @Override
    public void merge(@NotNull final String entityId, @NotNull final Task entity) {
        entity.setId(entityId);
        entityMap.put(entityId, entity);
    }

    @Override
    public void remove(@NotNull final String entityId) throws Exception {
        final boolean mapIsEmpty = entityMap.isEmpty();
        if(mapIsEmpty)throw new EntityListIsEmptyException();
        final boolean mapContainsKey = entityMap.containsKey(entityId);
        if(!mapContainsKey) throw new EntityNotExistException();
        else entityMap.remove(entityId);
    }

    @Override
    public void remove(@NotNull final String ownerId, @NotNull final String entityId) throws Exception {
        findOne(ownerId, entityId);
        remove(entityId);
    }

    @Override
    public void removeAll() throws Exception {
        final boolean mapIsEmpty = entityMap.isEmpty();
        if(mapIsEmpty) return;
        else entityMap.clear();
    }

    @Override
    public void removeAll(@NotNull final String ownerId) throws Exception {
        @NotNull final List<Task> list = findAll(ownerId);
        for (@NotNull final Task entity : list) {
            @NotNull final String entityId = entity.getId();
            remove(entityId);
        }
    }

}
