package com.shumov.tm.repository;

import com.shumov.tm.api.repository.ISessionRepository;
import com.shumov.tm.entity.Session;
import com.shumov.tm.exception.entity.EntityIsAlreadyExistException;
import com.shumov.tm.exception.entity.EntityListIsEmptyException;
import com.shumov.tm.exception.entity.EntityNotExistException;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class SessionRepository extends AbstractRepository<Session> implements ISessionRepository {

    @Override
    public @NotNull List<Session> findAll() {
        final boolean mapIsEmpty = entityMap.isEmpty();
        if(mapIsEmpty) return new ArrayList<>();
        else return new ArrayList<>(entityMap.values());
    }

    @NotNull
    @Override
    public Session findOne(@NotNull String entityId) throws Exception {
        final boolean mapIsEmpty = entityMap.isEmpty();
        if(mapIsEmpty) throw new EntityListIsEmptyException();
        final boolean mapContainsKey = entityMap.containsKey(entityId);
        if(mapContainsKey) return entityMap.get(entityId);
        else throw new EntityNotExistException();
    }

    @Override
    public void persist(@NotNull Session entity) throws Exception {
        @NotNull final String entityId = entity.getId();
        final boolean mapContainsKey = entityMap.containsKey(entityId);
        if(mapContainsKey) throw new EntityIsAlreadyExistException();
        else entityMap.put(entity.getId(), entity);
    }

    @Override
    public void merge(@NotNull String entityId, @NotNull Session entity) {
        entity.setId(entityId);
        entityMap.put(entityId, entity);
    }

    @Override
    public void remove(@NotNull String entityId) throws Exception {
        final boolean mapIsEmpty = entityMap.isEmpty();
        if(mapIsEmpty)throw new EntityListIsEmptyException();
        final boolean mapContainsKey = entityMap.containsKey(entityId);
        if(!mapContainsKey) throw new EntityNotExistException();
        else entityMap.remove(entityId);
    }

    @Override
    public void removeAll() throws Exception {
        final boolean mapIsEmpty = entityMap.isEmpty();
        if(mapIsEmpty) return;
        else entityMap.clear();
    }

    public boolean contains(@NotNull final String id){
        return entityMap.containsKey(id);
    }
}
