package com.shumov.tm.repository;

import com.shumov.tm.api.entity.Entity;
import com.shumov.tm.api.repository.IRepository;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

import java.util.LinkedHashMap;
import java.util.Map;

@NoArgsConstructor
public abstract class AbstractRepository<T extends Entity> implements IRepository<T> {

    @NotNull
    protected final Map<String, T> entityMap = new LinkedHashMap<>();

}
