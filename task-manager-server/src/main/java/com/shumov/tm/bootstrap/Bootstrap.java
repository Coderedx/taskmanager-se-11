package com.shumov.tm.bootstrap;



import com.shumov.tm.api.service.*;
import com.shumov.tm.endpoint.*;
import com.shumov.tm.enumerate.Role;
import com.shumov.tm.service.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import javax.xml.ws.Endpoint;
import java.util.*;

@NoArgsConstructor
@Getter
@Setter
public class Bootstrap implements ServiceLocator {

    @NotNull
    private final IProjectService projectService = new ProjectService(this);
    @NotNull
    private final ITaskService taskService = new TaskService(this);
    @NotNull
    private final IUserService userService = new UserService();
    @NotNull
    private final IDomainService domainService = new DomainService(this);
    @NotNull
    private final ISessionService sessionService = new SessionService(this);
    @NotNull
    private final IAdminService adminService = new AdminService(this);

    public void init() throws Exception {
        initUsers();
        publishEndpoints();
    }

    private void publishEndpoints(){
        Endpoint.publish("http://localhost:8080/ProjectEndpoint?wsdl",
                new ProjectEndpoint(projectService, sessionService));
        Endpoint.publish("http://localhost:8080/TaskEndpoint?wsdl",
                new TaskEndpoint(taskService, sessionService));
        Endpoint.publish("http://localhost:8080/UserEndpoint?wsdl",
                new UserEndpoint(userService, sessionService));
        Endpoint.publish("http://localhost:8080/SessionEndpoint?wsdl",
                new SessionEndpoint(sessionService));
        Endpoint.publish("http://localhost:8080/AdminEndpoint?wsdl",
                new AdminEndpoint(adminService, sessionService));

        System.out.println("http://localhost:8080/ProjectEndpoint?wsdl");
        System.out.println("http://localhost:8080/TaskEndpoint?wsdl");
        System.out.println("http://localhost:8080/UserEndpoint?wsdl");
        System.out.println("http://localhost:8080/SessionEndpoint?wsdl");
        System.out.println("http://localhost:8080/AdminEndpoint?wsdl");

    }

    private void initUsers() throws Exception  {
        userService.createNewUser("user", "user", Role.USER);
        userService.createNewUser("admin", "admin", Role.ADMIN);
    }

}
