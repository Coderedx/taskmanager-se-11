package com.shumov.tm;

import com.shumov.tm.bootstrap.Bootstrap;
import org.jetbrains.annotations.NotNull;
import org.reflections.Reflections;

import java.util.Set;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ) {
        Bootstrap bootstrap = new Bootstrap();
        try {
            bootstrap.init();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

